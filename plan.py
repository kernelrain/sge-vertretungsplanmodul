"""Parser for the SGE-Vertretungsplan."""

from bs4 import BeautifulSoup
import urllib.request
import urllib.parse
import re
import datetime


BASE_URL = "http://terminal.siegtal-gymnasium.de/"
TABLE_ROW_SELECTOR = re.compile(r"vertretung_entry([0-1]|_material)")


def _fetch_page(parameters):
    """Fetches the page given by the URL as BeautifulSoup object."""
    url_object = urllib.request.urlopen(BASE_URL, parameters)
    soup = BeautifulSoup(url_object.read())
    return soup


def _construct_date_parameters(date):
    """Constructs the POST parameters for the given date."""
    date_string = date.isoformat()
    mapping = {
        'newday': date_string,
        'option': 'com_school_vertretung',
        'catid': '',
        'view': 'vertretungen',
        'day': ''
    }
    return urllib.parse.urlencode(mapping).encode("utf-8")


def _get_next_week_day(today):
    """ calculates the next weekday as datetime.date object.

    If the current weekday is Monday - Thursday, the time_delta is 1.
    Else, if it's Friday or weekend, the next Monday is calculated by
    time_delta = 7-current_weekday (current_weekday starts 0 for Monday)"""
    time_delta_days = 1 if today.weekday() <= 3 else 7 - today.weekday()
    timedelta = datetime.timedelta(time_delta_days)
    return today + timedelta


def _parse_vertretung_news(soup):
    """Extracts the news from the <marquee>-tag."""
    news_div = soup.find("div", class_="vertretung_news")
    return news_div.text.strip() if news_div else None


def _parse_vertretung_plan(soup):
    """Extracts the plan out of the given soup.

    Returns a dictionary of following format:
    The class names are represented by the dictionary's keys.
    The corresponding values are lists of the entries.
    An entry is represented by a dictionary containing the entry's attributes.
    """
    content_pane = soup.findAll("div", {"class": "contentpane"})[0]

    # Get forms
    html_form_headings = content_pane.findAll("h3",
                                              {"class": "vertretung_headline"})
    form_headings = [
        html_form_heading.string[
            html_form_heading.string.find(":") +
            2:] for html_form_heading in html_form_headings]

    # Get tables
    html_tables = content_pane.findAll("table", {"class": "vertretung"})

    tables = []

    # Iterate over each table
    # One table per form
    for html_table in html_tables:
        html_rows = html_table.findAll("tr",
                                       {"class": TABLE_ROW_SELECTOR})
        rows = []
        for html_row in html_rows:
            row = {}
            # Check whether entry has material
            if html_row.get("class")[0] == "vertretung_entry_material":
                sample_cell = html_row.findAll("td")[0]
                onclick = sample_cell.get("onclick")
                # onclick attribute has following format
                # location.href='...'
                # extract link and convert it to full qualified url
                onclick_link = BASE_URL + \
                    onclick[onclick.find("=") + 2:len(onclick) - 1]
                # Add to row dictionary
                row["material"] = onclick_link
            else:
                row["material"] = None

            for html_data_cell in html_row.findAll("td"):
                class_name = html_data_cell.get("class")[0]
                # Write entry to row dictionary
                # But except button as it's always None
                if not class_name == "vertretung_buttons":
                    # remove 'vertretung_' from each attribute name
                    attribute_name = class_name[class_name.find('_') + 1:]
                    row[attribute_name] = html_data_cell.text
            rows.append(row)
        tables.append(rows)

    plan = dict(zip(form_headings, tables))
    return plan


def get_plan():
    """ Returns the plan for two days. """
    today = datetime.date.today()

    first_day = today if today.weekday() < 5 else _get_next_week_day(today)
    second_day = _get_next_week_day(first_day)
    dates = first_day, second_day

    # create page soup generator
    page_soups = [
        _fetch_page(
            _construct_date_parameters(date)) for date in dates]
    # create plan_dict generator
    plan_dicts = (_parse_vertretung_plan(page_soup)
                  for page_soup in page_soups)
    # zip dates and plans
    plan = dict(zip((date.isoformat() for date in dates), plan_dicts))
    # add news
    news = _parse_vertretung_news(page_soups[0])

    return Plan(plan, news)


class Plan:
    """Represents a plan object and allows filtering by date and form."""

    def __init__(self, plan_dict, news):
        self.plan = plan_dict
        self.news = news

    def filter_by_day(self, date):
        """ Returns the plan for the given date.

        date has to be a datetime.date object and can either be today
        or the following weekday. """
        iso_date = date.isoformat()
        if iso_date not in self.plan:
            raise ValueError("Invalid date")
        else:
            return Plan(self.plan[iso_date], self.news)

    def filter_by_form(self, name):
        """ Returns the plan filtered by form name.

        form name should be passed without a leading zero, e.g. '5B' """
        form_filter = (name, '0' + name)
        result = {date: {form_name: plan for form_name, plan in day_plan.items(
        ) if form_name in form_filter} for date, day_plan in self.plan.items()}
        return Plan(result, self.news)

    def __str__(self):
        tab = 2 * ' '
        full_string = ""
        for date, day_plan in self.plan.items():
            if day_plan:
                full_string += date + ':' + '\n'
                for grade_name, grade_plan in day_plan.items():
                    full_string += tab + grade_name + ':' + '\n'
                    entries = []
                    for entry in grade_plan:
                        entries.append(
                            '\n'.join(
                                '%s%s: %s' %
                                (2 *
                                 tab,
                                 attribute,
                                 value) for attribute,
                                value in entry.items() if value))
                    full_string += '\n\n'.join(entries) + '\n'
        return full_string
